Format: 1.0
Source: agl-libafb-helpers
Binary: agl-libafb-helpers-bin, agl-libafb-helpers-dev
Architecture: any
Version: 2.0-0
Maintainer: romain.forlot <romain.forlot@iot.bzh>
Standards-Version: 3.8.2
Homepage: https://gerrit.automotivelinux.org/gerrit/src/libafb-helpers
Build-Depends: debhelper (>= 5),
 cmake,
 dpkg-dev,
 pkg-config,
 libjson-c-dev,
 libsystemd-dev,
 libcurl4-openssl-dev,
 libqt5websockets5-dev,
 agl-app-framework-binder-bin,
 agl-app-framework-binder-dev,
DEBTRANSFORM-RELEASE: 1
Files:
 libafb-helpers_2.0.tar.gz
